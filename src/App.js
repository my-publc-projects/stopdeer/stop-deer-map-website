import React from 'react';
import Main from './Components/Main'
import About from './Components/About'
import { BrowserRouter as Router, Route } from "react-router-dom";
import LocaleContext, { allLocales } from "./Context/LocaleContext";
// import logo from './logo.svg';
import withTracker from './withTracker';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      lang: "en"
    }
  }

  render() {
    return (

      <LocaleContext.Provider value={allLocales[this.state.lang]}>
        <Router>
          <div>
            <Route exact path="/" component={withTracker(Main)} />
            <Route path="/about" component={withTracker(About)} />
          </div>
        </Router>
      </LocaleContext.Provider>
    );
  }
}

export default App;
