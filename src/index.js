import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { CriticalCSSProvider, StyleRegistry } from 'react-critical-css'; // <-

const styleRegistry = new StyleRegistry(); // create new style registry

ReactDOM.render(
    <CriticalCSSProvider registry={styleRegistry}> 
        <App />
    </CriticalCSSProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
