import React from 'react';
import { Link } from 'react-router-dom'

import { withStyles } from 'react-critical-css'; // import 'withStyles'

import s3 from 'bootstrap/dist/css/bootstrap.css';

class About extends React.Component {
    render() {
        // document.body.style.overflow = null;
        let link = `https://t.me/${process.env.REACT_APP_BOT_NAME}`;
        return (
            <div className="container">
                <div className="col-lg-8 mx-auto">
                    <span className={`h1 mb-4 w-100 text-center text-Default`}></span>
                    <h1>Про проект:</h1>
                    <p className="lead">
                        Все почалось з невдоволення тим, як люди ігнорують правила дорожнього руху і цим самим проявляють неповагу до пішоходів, ставлячи свої автівки в найнепередбачуваніших для цього місцях.
                        Було створено бот в месенджері Tlegram, який допомагає реєструвати дані про правопорушення пов'язані з паркуванням та цей веб сайт, який дає можливіть спостерігати за масштабами цих правопорушеннь.
                        Все ще в розробці тому буду радий вислухати зауваження та пропозиції.
                        Скоро тут з'явиться інформація, як зв'язатись зі мною.
                        <br />
                        Детальний опис функціоналу тут: <a href="/slides-deck.html">презентація</a>
                        <br />
                        Щоб побачити карту треба перейти на <Link to="/">головну сторінку </Link>
                        <br />
                        посиллання на бот в телеграм: <a href={link}>{process.env.REACT_APP_BOT_NAME}</a>
                        <br />
                        Мій персональний сайт: <a href="https://rmnnk.pp.ua">тиць</a>
                    </p>
                </div>
            </div>
        );
    };
}

export default withStyles(s3)(About)