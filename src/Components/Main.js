import React from 'react';
import NavBar from './NavBar'
import MapView from "./MapViev"
import ModeContext, { allModes } from "../Context/ModeContext";
import DataContext from '../Context/DataContext';
import Sockette from 'sockette';
import moment from 'moment';
import logo from '../tlogo.webp'; // Tell Webpack this JS file uses this image
import slogo from '../slogo.webp'; // Tell Webpack this JS file uses this image

import { Link } from 'react-router-dom'
let ws = null;

class Main extends React.Component {
    constructor() {
        super();
        this.state = {
            mode: allModes.setTime,
            isOpen: false,
            data: [],
            dateFilter: {},
            queuedQuery: false
        }
        this.dateFilterUpdated = this.dateFilterUpdated.bind(this);
        this.getRectords = this.getRectords.bind(this);
    }

    componentWillUnmount() {
        if (this.state.isOpen)
            ws.close();
    }

    componentDidMount() {
        console.log("mount!!!");
        ws = new Sockette(process.env.REACT_APP_WEBSOCKET_ENDPOINT, {
            // timeout: 5e3,
            maxAttempts: 10,
            onopen: e => {
                console.log('Connected!', e);
                this.setState({ isOpen: true });
                if (this.state.queuedQuery) {
                    console.log("some date already set...")
                    this.setState({ queuedQuery: false });
                    this.getRectords();
                }
            },
            onmessage: e => {
                console.log('Received:', e)
                try {
                    let parsedMessage = JSON.parse(e.data);
                    // console.log(message);
                    // console.log(typeof parsedMessage)
                    console.log("handle action", parsedMessage.action);
                    if (typeof parsedMessage.action === "string") {
                        switch (parsedMessage.action) {
                            case "realTimeData":
                                let data = this.state.data;
                                data.push(parsedMessage.data);
                                // console.log(data);
                                this.setState({ data: data });
                                break;
                            case "getRecordsData":
                                // console.log(parsedMessage.data);
                                this.setState({ data: parsedMessage.data });
                                break;
                            case "setRealTime":
                                console.log('Set Real Time Data Resoponse');
                                break;
                            default:
                                console.log("unhandled action", parsedMessage.action);
                                break;
                        }
                    } else {
                        console.log("message with no action", parsedMessage);
                    }

                } catch (err) {
                    console.error(err.message);
                    console.error(err.stack);
                }
            },
            onreconnect: e => console.log('Reconnecting...', e),
            onmaximum: e => console.log('Stop Attempting!', e),
            onclose: e => {
                console.log('Closed!', e);
                this.setState({ isOpen: false })
            },
            onerror: e => console.log('Error:', e)
        });

        this.dateFilterUpdated = function (startDate, endDate) {
            console.log("date updated");
            this.setState({
                dateFilter: {
                    startTime: startDate.hour(0).minute(0).second(0).unix() * 1000,
                    endTime: endDate.hour(23).minute(59).second(59).unix() * 1000
                }
            });

            if (this.state.isOpen)
                this.getRectords();
            else
                this.setState({ queuedQuery: true });
        }
    }

    getRectords() {
        if (this.state.queuedQuery) {
            this.setState({ queuedQuery: false });
        }
        ws.json({
            action: "getRecords",
            data: this.state.dateFilter
        });
    }

    dateFilterUpdated(startDate, endDate) {
        console.log("date updated but not mounted yet");
        this.setState({
            dateFilter: {
                startTime: startDate.hour(0).minute(0).second(0).unix() * 1000,
                endTime: endDate.hour(23).minute(59).second(59).unix() * 1000
            },
            queuedQuery: true
        });
    }

    render() {
        let link = `https://t.me/${process.env.REACT_APP_BOT_NAME}`;
        return (
            <DataContext.Provider value={this.state.data}>
                <ModeContext.Provider value={this.state.mode}>
                    <div className="App">
                        <div style={{
                            zIndex: 10,
                            top: "1em",
                            left: "1em",
                            position: "absolute",
                        }}>
                            <NavBar
                                onModeChange={(mode) => {
                                    if (this.state.isOpen) {
                                        console.log("on mode change");
                                        if (mode === allModes.realTime) {
                                            let startDate = moment().hour(0).minute(0).second(0);
                                            startDate.day(startDate.day() - 1)
                                            let endDate = moment();
                                            this.setState({
                                                dateFilter: {
                                                    startTime: startDate.unix() * 1000,
                                                    endTime: endDate.unix() * 1000
                                                }
                                            });
                                        }

                                        this.getRectords();

                                        ws.json({
                                            action: "setRealTime",
                                            data: mode === allModes.realTime
                                        });
                                    }

                                    this.setState({ mode: mode })
                                }}
                                onDateChange={(startDate, endDate) => { this.dateFilterUpdated(startDate, endDate) }}
                            />
                        </div>

                        <Link to="/about"> <img style={
                            {
                                bottom: "5em",
                                left: "1em",
                                position: "absolute",
                                height: "3em",
                                /* width: 3em; */
                                zIndex: 5,
                            }
                        } src={slogo} alt="Logo" /></Link>

                        <a href={link} target="_blank" rel="noopener noreferrer">
                            <img style={
                                {
                                    bottom: "1.5em",
                                    left: "1em",
                                    position: "absolute",
                                    height: "3em",
                                    /* width: 3em; */
                                    zIndex: 5,
                                }
                            } src={logo} alt="Logo" />
                        </a>

                        <div style={{
                            backgroundColor: "#282c34",
                            width: '100%',
                            /* min-height: 100vh; */
                            height: "100vh",
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: "center",
                            color: "white",
                        }} >
                            <MapView />
                        </div>
                    </div>
                </ModeContext.Provider>
            </DataContext.Provider >
        );
    }
}

export default Main;
