import React from 'react';
import GoogleMapReact from 'google-map-react';
import Marker from "./Marker"
import ModeContext, { allModes } from "../Context/ModeContext";
import DataContext from '../Context/DataContext';


const mapOptions = {
    styles: [
        // { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
        // {
        //     elementType: 'labels.text.stroke',
        //     // stylers: [{ color: '#242f3e' }]
        //     stylers: [{ visibility: 'off' }]

        // },
        // {
        //     featureType: 'poi.business',
        //     stylers: [{ visibility: 'off' }]
        // },
        {
            featureType: 'transit',
            elementType: 'labels.icon',
            stylers: [{ visibility: 'off' }]
        },

        // {
        //     elementType: 'labels.text.fill',
        //     // stylers: [{ color: '#746855' }]
        //     stylers: [{ visibility: 'off' }]
        // },
        // {
        //     featureType: 'administrative.locality',
        //     // elementType: 'labels.text.fill',
        //     // stylers: [{ color: '#d59563' }]
        //     stylers: [{ visibility: 'off' }]
        // },
        {
            featureType: 'poi',
            // elementType: 'labels.text.fill',
            //   stylers: [{color: '#d59563'}]
            stylers: [{ visibility: 'off' }]
        },
        // {
        //     featureType: 'poi.park',
        //     elementType: 'geometry',
        //     stylers: [{ color: '#263c3f' }]
        // },
        // {
        //     featureType: 'poi.park',
        //     elementType: 'labels.text.fill',
        //     stylers: [{ color: '#6b9a76' }]
        // },
        // {
        //     featureType: 'road',
        //     elementType: 'geometry',
        //     stylers: [{ color: '#38414e' }]
        // },
        // {
        //     featureType: 'road',
        //     elementType: 'geometry.stroke',
        //     stylers: [{ color: '#212a37' }]
        // },
        // {
        //     featureType: 'road',
        //     elementType: 'labels.text.fill',
        //     stylers: [{ color: '#9ca5b3' }]
        // },
        // {
        //     featureType: 'road.highway',
        //     elementType: 'geometry',
        //     stylers: [{ color: '#746855' }]
        // },
        // {
        //     featureType: 'road.highway',
        //     elementType: 'geometry.stroke',
        //     stylers: [{ color: '#1f2835' }]
        // },
        // {
        //     featureType: 'road.highway',
        //     elementType: 'labels.text.fill',
        //     stylers: [{ color: '#f3d19c' }]
        // },
        // {
        //     featureType: 'transit',
        //     elementType: 'geometry',
        //     stylers: [{ color: '#2f3948' }]
        // },
        // {
        //     featureType: 'transit.station',
        //     elementType: 'labels.text.fill',
        //     stylers: [{ color: '#d59563' }]
        // },
        // {
        //     featureType: 'water',
        //     elementType: 'geometry',
        //     stylers: [{ color: '#17263c' }]
        // },
        // {
        //     featureType: 'water',
        //     elementType: 'labels.text.fill',
        //     stylers: [{ color: '#515c6d' }]
        // },
        // {
        //     featureType: 'water',
        //     elementType: 'labels.text.stroke',
        //     stylers: [{ color: '#17263c' }]
        // }
    ]
};

class SimpleMap extends React.Component {
    static defaultProps = {
        center: { lat: 50.45466, lng: 30.5238 },
        zoom: 12
    };
    renderMap(data, isHeatMap) {
        let filteredData = data.filter(toFilter => { return toFilter.id && toFilter.saved_location });

        var heatMapData = {
            positions: [],
            options: {
                radius: 30,
                opacity: 0.8,
            },
        };
        var markers = [];

        if (isHeatMap) {
            heatMapData.positions = filteredData.map(place => ({
                lat: place.saved_location.latitude,
                lng: place.saved_location.longitude,
                weight: 3,
                // weight: Math.floor(Math.random() * Math.floor(5)),
            }));
        } else {
            const rowLen = filteredData.length;
            markers = filteredData.map((item, i) => {
                if (rowLen === i + 1) {
                    // last one
                    return <Marker animate={true} key={item.id} lat={item.saved_location.latitude} lng={item.saved_location.longitude} />;
                } else {
                    // not last one
                    return <Marker key={item.id} lat={item.saved_location.latitude} lng={item.saved_location.longitude} />;
                }
            });
        }



        return (<GoogleMapReact
            bootstrapURLKeys={{
                key: process.env.REACT_APP_API_KEY,
                libraries: ['visualization'],
                language: 'ua',
                region: 'ua',
            }}
            // layerTypes={['TrafficLayer']}
            defaultCenter={this.props.center}
            defaultZoom={this.props.zoom}
            heatmapLibrary={true}
            heatmap={heatMapData}
            options={mapOptions}
        >
            {markers}
        </GoogleMapReact>);
    }



    render() {
        return (
            <DataContext.Consumer>
                {data => (
                    <ModeContext.Consumer>
                        {mode => { return this.renderMap(data, mode === allModes.heatMap) }}
                    </ModeContext.Consumer>)}
            </DataContext.Consumer>
        );
    }
}

export default SimpleMap