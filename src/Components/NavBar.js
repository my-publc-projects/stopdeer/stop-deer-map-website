import React from 'react';
import Button from '@material-ui/core/Button';
// import Grid from '@material-ui/core/Grid';
import ModeContext, { allModes } from "../Context/ModeContext";
import LocaleContext from "../Context/LocaleContext";
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import moment from 'moment'
import { withStyles } from 'react-critical-css'; // import 'withStyles'

import s1 from 'react-dates/lib/css/_datepicker.css';

const buttonStyle = {
    marginRight: "1em",
    marginBottom: "1em",
    marginTop: "1em",
}

class NavBar extends React.Component {
    constructor() {
        super();

        this.state = {
            startDate: moment().hour(0).day(moment().day() - 7),
            endDate: moment().hour(0),
            today: moment().hour(0).minute(0).second(0)
        }

        console.log(this.state.startDate.toString())
        console.log(this.state.endDate.toString())

        this.changeMode = this.changeMode.bind(this);
    }

    componentDidMount() {
        if (this.props.onDateChange && this.state.startDate && this.state.endDate) {
            this.state.endDate.day(this.state.endDate.day());
            this.props.onDateChange(this.state.startDate, this.state.endDate)
        }
    }

    changeMode(mode) {
        console.log("state changed to", mode);
        if (this.props.onModeChange) {
            this.props.onModeChange(mode);
        }
    }

    render() {
        return (
            <LocaleContext.Consumer>
                {locale => (
                    <ModeContext.Consumer>
                        {mode => (
                            <div >
                                <Button
                                    variant="contained"
                                    onClick={() => {

                                        this.changeMode(allModes.realTime)
                                    }}
                                    color={mode === allModes.realTime ? "primary" : "default"}
                                    style={buttonStyle}>
                                    {locale.strings.real_time}
                                </Button>
                                <Button
                                    variant="contained"
                                    onClick={() => { this.changeMode(allModes.setTime) }}
                                    color={mode === allModes.setTime ? "primary" : "default"}
                                    style={buttonStyle}>
                                    {locale.strings.set_time}
                                </Button>
                                <Button
                                    variant="contained"
                                    onClick={() => { this.changeMode(allModes.heatMap) }}
                                    color={mode === allModes.heatMap ? "primary" : "default"}
                                    style={buttonStyle}>
                                    {locale.strings.heat_map}
                                </Button>

                                {mode === allModes.heatMap || mode === allModes.setTime ?
                                    <DateRangePicker
                                        isOutsideRange={(date) => {
                                            // console.log(`${date.format()} - ${this.state.today.format()}  ${date.diff(this.state.today, 'days')}`)
                                            return date.diff(this.state.today, 'days') > 0
                                        }}
                                        minimumNights={0}
                                        firstDayOfWeek={1}
                                        startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                        startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                        endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                        endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                        onDatesChange={({ startDate, endDate }) => {
                                            if (startDate) {
                                                console.log("set start", startDate);
                                                this.setState({ startDate });
                                            } else {
                                                console.log("startDate is undefined")
                                            }

                                            if (endDate) {
                                                console.log("set end", endDate);
                                                this.setState({ endDate });
                                            } else {
                                                console.log("endDate is undefined")
                                                endDate = this.state.endDate;
                                            }

                                            if (this.props.onDateChange && startDate && endDate) {
                                                // let s = startDate || this.state.startDate;
                                                this.props.onDateChange(startDate, endDate);
                                            }
                                        }} // PropTypes.func.isRequired,
                                        focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                        onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                    />
                                    : null}
                            </div>
                        )}
                    </ModeContext.Consumer>
                )}
            </LocaleContext.Consumer>
        );
    }
}

export default withStyles(s1)(NavBar)


