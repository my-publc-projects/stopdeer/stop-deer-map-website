import React from 'react';

export const allModes = {
    heatMap: "heat_map",
    realTime: "real_time",
    setTime: "set_time",
};

const ModeContext = React.createContext(allModes.setTime);

export default ModeContext;