import React from 'react';

const uaLocale =
{
    locale: "ua",
    strings: {
        real_time: "в реальному часі",
        heat_map: "теплова карта",
        set_time: "фільтр часу"
    } 
};

const enLocale =
{
    locale: "en",
    strings: {
        real_time: "real time",
        heat_map: "heat map",
        set_time: "set time"
    }
};

export const allLocales = {
    ua: uaLocale,
    en: enLocale,
};

const LocaleContext = React.createContext(allLocales.ua);

export default LocaleContext;